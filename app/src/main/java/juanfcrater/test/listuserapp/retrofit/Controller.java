package juanfcrater.test.listuserapp.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import juanfcrater.test.listuserapp.pojo.WebObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Controlador de retrofit que lee la api para obtener los datos, usa Gson para traducir el json al
// objeto Pojo de Java
public class Controller implements Callback<WebObject> {

    private static final String BASE_URL = "https://38f3f7db-5295-4c4f-a1e0-812bce1bd30d.mock.pstmn.io/api/v1/";
    private ControllerListener listener;

    //Metodo de la llamada, setea el listener para mantener la comunicacion
    public void start(ControllerListener listener) {
        this.listener = listener;
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        WebService webService = retrofit.create(WebService.class);

        Call<WebObject> call = webService.getUser();
        call.enqueue(this);

    }

    //Cuando responde, devuelve solo el listado de usuarios
    @Override
    public void onResponse(Call<WebObject> call, Response<WebObject> response) {
        if(response.isSuccessful()) {
            WebObject webObject = response.body();
            listener.actUsers(webObject.getUsers());
        } else {
            listener.OnFail();
        }
    }

    @Override
    public void onFailure(Call<WebObject> call, Throwable t) {
        listener.OnFail();
    }
}