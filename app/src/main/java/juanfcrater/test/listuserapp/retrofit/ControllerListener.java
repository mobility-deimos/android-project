package juanfcrater.test.listuserapp.retrofit;

import java.util.List;

import juanfcrater.test.listuserapp.pojo.User;

public interface ControllerListener {
    //Listener que conecta el repositoria al WebService
    void actUsers(List<User> usuarios);

    void OnFail();
}
