package juanfcrater.test.listuserapp.retrofit;

import juanfcrater.test.listuserapp.pojo.WebObject;
import retrofit2.Call;
import retrofit2.http.GET;

public interface WebService {
    //Referencia a punto de lectura en la API
    @GET("users")
    Call<WebObject> getUser();

}
