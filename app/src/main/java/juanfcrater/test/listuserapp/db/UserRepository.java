package juanfcrater.test.listuserapp.db;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import juanfcrater.test.listuserapp.pojo.User;
import juanfcrater.test.listuserapp.retrofit.Controller;
import juanfcrater.test.listuserapp.retrofit.ControllerListener;

public class UserRepository implements ControllerListener {

    private final RepositoryListener listener;
    private UserDao mUserDao;
    private LiveData<List<User>> mAllUsers;

    public UserRepository(Application application, RepositoryListener listener) {
        this.listener = listener;
        UserRoomDatabase db = UserRoomDatabase.getDatabase(application);
        mUserDao = db.userDao();
        mAllUsers = mUserDao.getAlphabetizedUsers();
        Controller controller = new Controller();
        controller.start(this);
    }

    public LiveData<List<User>> getAllUsers() {
        return mAllUsers;
    }

    public void insert(final User user) {
        UserRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mUserDao.insert(user);
            }
        });
    }

    @Override
    public void actUsers(List<User> usuarios) {
        for (int i = 0; i<usuarios.size();i++)
            insert(usuarios.get(i));
        listener.hasSucced();
    }

    @Override
    public void OnFail() {
        listener.hasFail();
    }
}
