package juanfcrater.test.listuserapp.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


//Objeto de la base de datos
public class WebObject {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("users")
    @Expose
    private List<User> users = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}