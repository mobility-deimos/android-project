package juanfcrater.test.listuserapp.pojo;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


//Entidad usuario, se usa para como POJO, traductor de GSON y elemento de la base de datos de ROOM
@Entity(tableName = "user_table")
public class User {

    @NonNull
    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    String name;

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "email")
    @SerializedName("email")
    @Expose
    String email;


    @NonNull
    @ColumnInfo(name = "profile")
    @SerializedName("profile")
    @Expose
    String profile;

    public User(String name, String email, String profile)
    {
        this.name = name;
        this.email = email;
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
