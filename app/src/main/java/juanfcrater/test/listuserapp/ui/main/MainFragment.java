package juanfcrater.test.listuserapp.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import juanfcrater.test.listuserapp.R;
import juanfcrater.test.listuserapp.pojo.User;
import juanfcrater.test.listuserapp.ui.main.listElement.RecyclerAdapter;
import juanfcrater.test.listuserapp.viewmodel.MainViewModel;

/*
Fragment principal de la vista, gestiona el listado y la conexion con el viewmodel
 */

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private List<User> listUser = new ArrayList<>();
    private RecyclerView listUserRecycler;
    private RecyclerAdapter adapter;
    private ProgressBar progressBar;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }
    /*
    Se obtiene la referencia al listado en el xml y se crea el adapter para setear
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listUserRecycler = view.findViewById(R.id.recycler);
        progressBar =  view.findViewById(R.id.progressBar);
        listUserRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RecyclerAdapter(listUser);
        listUserRecycler.setAdapter(adapter);
    }

    /*
        La activity se usa como contexto para el viewmodel, el fragment solo es un intermediario
         para separar vista de viewmodel, solo conectados por el adapter
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.getAllUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable final List<User> words) {
                adapter.setUserList(words);
            }
        });
        mViewModel.getMsg().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
            }
        });

        mViewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                progressBar.setVisibility(aBoolean?View.GONE:View.VISIBLE);
            }
        });

    }
}
