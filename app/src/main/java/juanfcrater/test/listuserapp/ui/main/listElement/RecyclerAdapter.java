package juanfcrater.test.listuserapp.ui.main.listElement;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import juanfcrater.test.listuserapp.R;
import juanfcrater.test.listuserapp.pojo.User;

//Adapter basico para el listado de la vista. Solo muestra la informacion de los usuarios
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CustomViewHolder> {
    private List<User> UserList;

    public RecyclerAdapter(List<User> UserList) {
        this.UserList = UserList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rowuseritem, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        if(UserList!=null && !UserList.isEmpty() && UserList.get(i) != null) {
            User user = UserList.get(i);
            if (user != null) {
                customViewHolder.name.setText(Html.fromHtml(user.getName()));
                customViewHolder.email.setText(Html.fromHtml(user.getEmail()));
                customViewHolder.profile.setText(Html.fromHtml(user.getProfile()));
            }
        }
    }

    @Override
    public int getItemCount() {
        return (UserList != null ? UserList.size() : 0);
    }


    public void setUserList(List<User> users){
        UserList = users;
        notifyDataSetChanged();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView name;
        protected TextView email;
        protected TextView profile;

        CustomViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.name);
            this.email = view.findViewById(R.id.email);
            this.profile = view.findViewById(R.id.profile);
        }
    }
}
