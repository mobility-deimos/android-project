package juanfcrater.test.listuserapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import juanfcrater.test.listuserapp.db.RepositoryListener;
import juanfcrater.test.listuserapp.db.UserRepository;
import juanfcrater.test.listuserapp.pojo.User;

public class MainViewModel extends AndroidViewModel implements RepositoryListener {

    //Referencia al repositorio, vista conoce repositorio atraves de su viewmodel
    private UserRepository repository;

    //LiveData con el listado de los usuarios
    private LiveData<List<User>> users;
    //Livedatas de informacion a usuario
    private MutableLiveData<Boolean> load = new MutableLiveData<>();
    private MutableLiveData<String> msg = new MutableLiveData<>();

    //Constructor de ViewModel, crea el repositorio pasando la referencia de contexto de la aplicacion y pide los usuarios al repositorio
    public MainViewModel(@NonNull Application application) {
        super(application);
        repository = new UserRepository(application, this);
        load.postValue(true);
        users = repository.getAllUsers();
    }

    //Metodo a quien llama el view para conocer los datos de usuario
    public LiveData<List<User>> getAllUsers()
    {
        return users;
    }

    public MutableLiveData<Boolean> getLoad()
    {
        return load;
    }

    public MutableLiveData<String> getMsg()
    {
        return msg;
    }

    @Override
    public void hasFail() {
        load.postValue(false);
        msg.postValue("Error");
    }

    @Override
    public void hasSucced() {
        load.postValue(false);
        msg.postValue("Success");
    }
}
